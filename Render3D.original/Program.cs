﻿using ConsoleCanvas;
using System.Drawing;
using System.Numerics;

namespace Render3D;

public class Program
{
    private const float _zfar = 110.0f;
    private const float _znear = 10.0f;
    private const float _λ = _zfar / (_zfar - _znear);
    private float _aspectRatio;
    private const float _fieldOfViewInDegrees = 90;
    private const float _fieldOfViewInRadians = _fieldOfViewInDegrees * (float)Math.PI / 180.0f;
    private static readonly float _inverseHalfFieldOfVision = 1.0f / (float)Math.Tan(_fieldOfViewInRadians / 2.0d);

    private int _displayWidth;
    private int _displayHeight;
    private float _rotationRadians;

    static void Main(string[] args)
    {
        new Program().Run();
    }

    public void Run()
    {
        using(var canvas= new Canvas())
        {
            _aspectRatio = (float)canvas.Buffer.Height / (float)canvas.Buffer.Width;
            for (var degrees = 0; degrees <= 360; degrees += 360 / 20)
            {
                _displayWidth = canvas.Buffer.Width;
                _displayHeight = canvas.Buffer.Height;
                _rotationRadians = (float)(degrees * Math.PI / 180.0f);
                DrawPattern(canvas);
                Console.SetCursorPosition(0, 0);
                Console.WriteLine(degrees.ToString());
                Console.ReadKey(true);
            }
        }
    }

    public void DrawPattern(Canvas canvas)
    {
        using (var graphics = Graphics.FromImage(canvas.Buffer))
        {
            graphics.Clear(Color.Black);
            //graphics.DrawLine(new Pen(Brushes.Green), 1, 1, canvas.Buffer.Width - 2, canvas.Buffer.Height - 2);
            //graphics.DrawLine(new Pen(Brushes.Green), canvas.Buffer.Width - 2, 1, 1, canvas.Buffer.Height - 2);
            //graphics.DrawEllipse(new Pen(Brushes.Orange), canvas.Buffer.Width / 2 - 50, canvas.Buffer.Height / 2 - 50, 100, 100);

            foreach (var coloredEdges in GenerateEdges())
            {
                graphics.DrawLines(new Pen(coloredEdges.color), coloredEdges.edges);
            }
        }
        canvas.Buffer.SetPixel(canvas.Buffer.Width / 2, canvas.Buffer.Height / 2, Color.Red);
        canvas.SetFrame();
    }

    private (Color color, Point[] edges)[] GenerateEdges()
    {
        var rtl = new Vector3(-25, -25, 25);
        var rtr = new Vector3( 25, -25, 25);
        var rbl = new Vector3(-25,  25, 25);
        var rbr = new Vector3( 25,  25, 25);

        var ftl = new Vector3(-25, -25, -25);
        var ftr = new Vector3( 25, -25, -25);
        var fbl = new Vector3(-25,  25, -25);
        var fbr = new Vector3( 25,  25, -25);

        var coloredEdges = new (Color, Point[])[] {
            new (Color.Red, new [] { projectTo2D(rtl), projectTo2D(rtr), projectTo2D(rbr), projectTo2D(rbl), projectTo2D(rtl) })
            , new (Color.Green, new [] { projectTo2D(ftl), projectTo2D(ftr), projectTo2D(fbr), projectTo2D(fbl), projectTo2D(ftl) })
            , new (Color.White, new [] { projectTo2D(ftl), projectTo2D(rtl) })
            , new (Color.White, new [] { projectTo2D(ftr), projectTo2D(rtr) })
            , new (Color.White, new [] { projectTo2D(fbl), projectTo2D(rbl) })
            , new (Color.White, new [] { projectTo2D(fbr), projectTo2D(rbr) })
        };

        return coloredEdges;
    }

    private Point projectTo2D(Vector3 point)
    {
        var rotation = new Matrix4x4(
             (float)Math.Cos(_rotationRadians), 0, (float)Math.Sin(_rotationRadians), 0,
                                             0, 1,                                 0, 0, 
            -(float)Math.Sin(_rotationRadians), 0, (float)Math.Cos(_rotationRadians), 0,
                                             0, 0,                                 0, 1
        );
        var translate = new Matrix4x4(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 80,
            0, 0, 0, 1
        );
        var projection = new Matrix4x4(
            _aspectRatio * _inverseHalfFieldOfVision, 0,                         0,  0,
            0,                                        _inverseHalfFieldOfVision, 0,  0,
            0,                                        0,                         _λ, -_λ*_znear,
            0,                                        0,                         1,  0
        );;

        var rotproj = projection * translate * rotation;

        var transformedPoint = new Vector4(
              point.X * rotproj.M11 + point.Y * rotproj.M12 + point.Z * rotproj.M13 + 1 * rotproj.M14
            , point.X * rotproj.M21 + point.Y * rotproj.M22 + point.Z * rotproj.M23 + 1 * rotproj.M24
            , point.X * rotproj.M31 + point.Y * rotproj.M32 + point.Z * rotproj.M33 + 1 * rotproj.M34
            , point.X * rotproj.M41 + point.Y * rotproj.M42 + point.Z * rotproj.M43 + 1 * rotproj.M44
        );

        return new Point(
            (int)((transformedPoint.X / transformedPoint.W + 1) / 2 * _displayWidth)
            , (int)((transformedPoint.Y / transformedPoint.W + 1) / 2 * _displayHeight)
        );
    }
}