﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Render3D.Utils
{
    public class TexturePixels
    {
        public Color[] Pixels { get; }
        public int Width { get; }
        public int Height { get; }

        public TexturePixels(Texture2D texture)
        {
            Width = texture.Width;
            Height = texture.Height;
            Pixels = new Color[Width * Height];
            texture.GetData(Pixels);
        }
    }
}
