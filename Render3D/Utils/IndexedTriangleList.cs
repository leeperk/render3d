﻿using System.Collections.Generic;
using System.Linq;

namespace Render3D.Utils
{
    public class IndexedTriangleList<T>
    {
        public IList<T> Vertices { get; set; }
        public IList<int> Indices { get; set; }
        public IList<bool> CullFlags { get; set; }

        public IndexedTriangleList(
            IList<T> vertices
            , IList<int> indices
        )
        {
            Vertices = vertices.ToList();
            Indices = indices.ToList();
            CullFlags = Enumerable.Repeat(false, indices.Count / 3).ToList();
        }
    }
}
