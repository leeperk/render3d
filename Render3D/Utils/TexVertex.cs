﻿namespace Render3D.Utils
{
    public class TexVertex
    {
        public Vec3 Position { get; set; }
        public Vec2 TextureCoord { get; set; }

        public TexVertex(Vec3 position, Vec2 textureCoord)
        {
            Position = position;
            TextureCoord = textureCoord;
        }

        public TexVertex InterpolateTo(TexVertex destination, double αPercent)
            => new TexVertex(
                Position.InterpolateTo(destination.Position, αPercent)
                , TextureCoord.InterpolateTo(destination.TextureCoord, αPercent)
            );

        public static TexVertex operator +(TexVertex lhs, TexVertex rhs)
            => new TexVertex(lhs.Position + rhs.Position, lhs.TextureCoord + rhs.TextureCoord);

        public static TexVertex operator -(TexVertex lhs, TexVertex rhs)
            => new TexVertex(lhs.Position - rhs.Position, lhs.TextureCoord - rhs.TextureCoord);

        public static TexVertex operator *(TexVertex lhs, double rhs)
            => new TexVertex(lhs.Position * rhs, lhs.TextureCoord * rhs);

        public static TexVertex operator /(TexVertex lhs, double rhs)
            => new TexVertex(lhs.Position / rhs, lhs.TextureCoord / rhs);
    }
}
