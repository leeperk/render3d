﻿namespace Render3D.Utils
{
    public class PerspectiveTransformer
    {
        private double _xFactor;
        private double _yFactor;

        public PerspectiveTransformer(double width, double height)
        {
            _xFactor = width / 2.0;
            _yFactor = height / 2.0;
        }

        public Vec3 Transform(Vec3 v)
        {
            var zInv = 1.0 / v.Z;
            v.X = (v.X * zInv + 1.0) * _xFactor;
            v.Y = (-v.Y * zInv + 1.0) * _yFactor;
            return v;
        }

        private Vec3 GetTransformed(Vec3 v)
        {
            return Transform(new Vec3(v));
        }
    }
}
