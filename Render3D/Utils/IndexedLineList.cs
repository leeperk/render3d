﻿using System.Collections.Generic;

namespace Render3D.Utils
{
    public class IndexedLineList
    {
        public IList<Vec3> Vertices { get; set; }
        public IList<int> Indices { get; set; }

        public IndexedLineList()
        {
            Vertices = new List<Vec3>();
            Indices = new List<int>();
        }
    }
}
