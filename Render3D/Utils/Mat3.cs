﻿using Render3D.Exceptions;
using System;

namespace Render3D.Utils
{
    public class Mat3
    {
        public double[,] Elements { get; }

        public Mat3()
        {
            Elements = new double[3, 3];
        }

        public Mat3(Mat3 other) : this()
        {
            Array.Copy(Elements, other.Elements, 9);
        }

        public Mat3(double[,] elements) : this()
        {
            if (elements.GetLength(0) != 3 || elements.GetLength(1) != 3)
            {
                throw new ArgumentException("Must be 3x3 array", nameof(elements));
            }

            Array.Copy(elements, Elements, 9);
        }

        public static Mat3 operator *(Mat3 lhs, double rhs)
        {
            for (var row = 0; row < 3; row++)
            {
                for (var column = 0; column < 3; column++)
                {
                    lhs.Elements[row, column] *= rhs;
                }
            }
            return lhs;
        }

        public static Mat3 operator *(Mat3 lhs, Mat3 rhs)
        {
            var result = new Mat3();
            for (var j = 0; j < 3; j++)
            {
                for (var k = 0; k < 3; k++)
                {
                    var sum = 0.0;
                    for (var i = 0; i < 3; i++)
                    {
                        sum += lhs.Elements[j, i] * rhs.Elements[i, k];
                    }
                    result.Elements[j, k] = sum;
                }
            }
            return result;
        }

        public static Mat3 Identity()
        {
            return new Mat3(new[,] {
                { 1.0, 0.0, 0.0 },
                { 0.0, 1.0, 0.0 },
                { 0.0, 0.0, 1.0 }
            });
        }

        public static Mat3 Scaling(double factor)
        {
            return new Mat3(new[,] {
                { factor, 0.0,    0.0    },
                { 0.0,    factor, 0.0    },
                { 0.0,    0.0,    factor }
            });
        }

        public static Mat3 RotationZ(double theta)
        {
            var sinTheta = Math.Sin(theta);
            var cosTheta = Math.Cos(theta);
            return new Mat3(new[,] {
                { cosTheta,  sinTheta, 0.0 },
                { -sinTheta, cosTheta, 0.0 },
                { 0.0,       0.0,      1.0 }
            });
        }

        public static Mat3 RotationY(double theta)
        {
            var sinTheta = Math.Sin(theta);
            var cosTheta = Math.Cos(theta);
            return new Mat3(new[,] {
                { cosTheta,  0.0, -sinTheta },
                { 0.0,       1.0, 0.0       },
                { sinTheta,  0.0, cosTheta  }
            });
        }

        public static Mat3 RotationX(double theta)
        {
            var sinTheta = Math.Sin(theta);
            var cosTheta = Math.Cos(theta);
            return new Mat3(new[,] {
                { 1.0,  0.0,      0.0       },
                { 0.0,  cosTheta,  sinTheta },
                { 0.0,  -sinTheta, cosTheta }
            });
        }
    }
}