﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Render3D.Extensions;
using System;

namespace Render3D.Utils
{
    public class Canvas
    {
        private readonly GraphicsDeviceManager _graphics;

        public Texture2D Texture { get; private set; }
        public int Width => _graphics.PreferredBackBufferWidth;
        public int Height => _graphics.PreferredBackBufferHeight;
        public Color[] PixelBuffer { get; }

        public Canvas(GraphicsDeviceManager graphics)
        {
            _graphics = graphics;
            _graphics.SynchronizeWithVerticalRetrace = false;

            if (_graphics.GraphicsDevice is null)
            {
                _graphics.ApplyChanges();
            }
            var size = Math.Min(
                _graphics.GraphicsDevice.Adapter.CurrentDisplayMode.Width - 60
                , _graphics.GraphicsDevice.Adapter.CurrentDisplayMode.Height - 60
            );
            graphics.PreferredBackBufferWidth = size;
            graphics.PreferredBackBufferHeight = size;
            graphics.ApplyChanges();

            PixelBuffer = new Color[Width * Height];
        }

        public void Initialize()
        {
            Texture = new Texture2D(_graphics.GraphicsDevice, Width, Height);
        }

        public void BeginFrame()
        {
            Clear(Color.Black);
        }

        public void EndFrame()
        {
            Texture.SetData(PixelBuffer);
        }

        public void Clear(Color color)
        {
            Array.Fill(PixelBuffer, color);
        }

        public void PutPixel(int x, int y, int r, int g, int b)
        {
            PutPixel(x, y, new Color(r, g, b));
        }

        public void PutPixel(int x, int y, Color color)
        {
            PixelBuffer[x + y * Width] = color;
        }

        public void DrawLine(Vec2 from, Vec2 to, Color color)
        {

            DrawLine(from.X, from.Y, to.X, to.Y, color);
        }

        public void DrawLine(double x1, double y1, double x2, double y2, Color color)
        {
            var dx = x2 - x1;
            var dy = y2 - y1;

            if (dy == 0.0 && dx == 0.0)
            {
                PutPixel((int)x1, (int)y1, color);
            }
            else if (Math.Abs(dy) > Math.Abs(dx))
            {
                if (dy < 0.0)
                {
                    double temp = x1;
                    x1 = x2;
                    x2 = temp;

                    temp = y1;
                    y1 = y2;
                    y2 = temp;
                }

                var m = dx / dy;
                double y = y1;
                int lastIntY = 0;
                for (double x = x1; y < y2; y += 1.0, x += m)
                {
                    lastIntY = (int)y;
                    PutPixel((int)x, lastIntY, color);
                }
                if ((int)y2 > lastIntY)
                {
                    PutPixel((int)x2, (int)y2, color);
                }
            }
            else
            {
                if (dx < 0.0)
                {
                    double temp = x1;
                    x1 = x2;
                    x2 = temp;

                    temp = y1;
                    y1 = y2;
                    y2 = temp;
                }

                var m = dy / dx;
                double x = x1;
                int lastIntX = 0;
                for (double y = y1; x < x2; x += 1.0, y += m)
                {
                    lastIntX = (int)x;
                    PutPixel(lastIntX, (int)y, color);
                }
                if ((int)x2 > lastIntX)
                {
                    PutPixel((int)x2, (int)y2, color);
                }
            }
        }

        public void DrawTriangle(Vec2 v0, Vec2 v1, Vec2 v2, Color color)
        {
            // v0, v1, v2 are screen coordinates with 0,0 in the lower left

            // sort vertices by Y
            if (v1.Y < v0.Y) { (v1, v0) = (v0, v1); }
            if (v2.Y < v1.Y) { (v2, v1) = (v1, v2); }
            if (v1.Y < v0.Y) { (v0, v1) = (v1, v0); }
            // v0, v1, v2 now in order by Y
            //   v0 = top (smallest) Y
            //   v1 = middle Y
            //   v2 = bottom (largest) Y

            if (v0.Y == v1.Y) // natural flat top
            {
                // sort top vertices by X
                if (v1.X < v0.X) { (v0, v1) = (v1, v0); }
                // v0 = top left X
                // v1 = top right X
                // v2 = bottom
                DrawFlatTopTriangle(v0, v1, v2, color);
            }
            else if (v1.Y == v2.Y) // natural flat bottom
            {
                // sort bottom vertices by X
                if (v2.X < v1.X) { (v1, v2) = (v2, v1); }
                // v0 = top
                // v1 = bottom left X
                // v2 = bottom right X
                DrawFlatBottomTriangle(v0, v1, v2, color);
            }
            else // general triangle
            {
                // find splitting vertex (vα)

                //               v0
                // -------------*--------- 1
                //       v1        vα      ↑
                // -----*---------*------- 
                //                   v2    ↓
                // -----------------*----- 0
                // The closer v1's Y is to v0's Y, the closer alphaSplit will be to 0.
                // The closer v1's Y is to v2's Y, the closer alphaSplit will be to 1.
                var αPercent =
                    (v1.Y - v0.Y) /
                    (v2.Y - v0.Y);

                // v2 - v0 is a vector pointing in the direction and length of v0->v2
                // (v2 - v0) * αPercent is a vector pointing in the direction of v0->v2, but with enough length to have the same y as v1.
                // v0 + (v2 - v0) * αPercent is the coordinate on the line v0->v2 that is horizontal to v1 (denoted with vα above).
                var vα = v0 + (v2 - v0) * αPercent;

                var splitLeft = v1;
                var splitRight = vα;
                if (v1.X > vα.X) // major left
                {
                    (splitLeft, splitRight) = (splitRight, splitLeft);
                }
                DrawFlatBottomTriangle(v0, splitLeft, splitRight, color);
                DrawFlatTopTriangle(splitLeft, splitRight, v2, color);
                DrawLine(splitLeft, splitRight, Color.Black );
            }
        }

        public void DrawTriangleTex(TexVertex v0, TexVertex v1, TexVertex v2, TexturePixels texturePixels)
        {
            // v0, v1, v2 are screen coordinates with 0,0 in the lower left

            // sort vertices by Y
            if (v1.Position.Y < v0.Position.Y) { (v1, v0) = (v0, v1); }
            if (v2.Position.Y < v1.Position.Y) { (v2, v1) = (v1, v2); }
            if (v1.Position.Y < v0.Position.Y) { (v0, v1) = (v1, v0); }
            // v0, v1, v2 now in order by Y
            //   v0 = top (smallest) Y
            //   v1 = middle Y
            //   v2 = bottom (largest) Y


            if (v0.Position.Y == v1.Position.Y) // natural flat top
            {
                // sort top vertices by X
                if (v1.Position.X < v0.Position.X) { (v0, v1) = (v1, v0); }
                // v0 = top left X
                // v1 = top right X
                // v2 = bottom
                DrawFlatTopTriangleTex(v0, v1, v2, texturePixels);
            }
            else if (v1.Position.Y == v2.Position.Y) // natural flat bottom
            {
                // sort bottom vertices by X
                if (v2.Position.X < v1.Position.X) { (v1, v2) = (v2, v1); }
                // v0 = top
                // v1 = bottom left X
                // v2 = bottom right X
                DrawFlatBottomTriangleTex(v0, v1, v2, texturePixels);
            }
            else // general triangle
            {
                // find splitting vertex (vα)

                //               v0
                // -------------*--------- 1
                //       v1        vα      ↑
                // -----*---------*------- 
                //                   v2    ↓
                // -----------------*----- 0
                // The closer v1's Y is to v0's Y, the closer αPercent will be to 0.
                // The closer v1's Y is to v2's Y, the closer αPercent will be to 1.
                var αPercent =
                    (v1.Position.Y - v0.Position.Y) /
                    (v2.Position.Y - v0.Position.Y);

                // (v2 - v0) is a vector pointing in the direction and length of v0->v2
                // (v2 - v0) * αPercent is a vector pointing in the direction of v0->v2, but with enough length to have the same y as v1.
                // v0 + (v2 - v0) * αPercent is the coordinate on the line v0->v2 that is horizontal to v1 (denoted with vα above).
                var vα = v0.InterpolateTo(v2, αPercent);

                var splitLeft = v1;
                var splitRight = vα;
                if (splitRight.Position.X < splitLeft.Position.X) // major left
                {
                    (splitLeft, splitRight) = (splitRight, splitLeft);
                }
                DrawFlatBottomTriangleTex(v0, splitLeft, splitRight, texturePixels);
                DrawFlatTopTriangleTex(splitLeft, splitRight, v2, texturePixels);
                DrawLine(splitLeft.Position, splitRight.Position, Color.Black);
            }
        }

        public void DrawTriangleTexWrap(TexVertex v0, TexVertex v1, TexVertex v2, TexturePixels texturePixels)
        {
            // v0, v1, v2 are screen coordinates with 0,0 in the lower left

            // sort vertices by Y
            if (v1.Position.Y < v0.Position.Y) { (v0, v1) = (v1, v0); }
            if (v2.Position.Y < v1.Position.Y) { (v1, v2) = (v2, v1); }
            if (v1.Position.Y < v0.Position.Y) { (v0, v1) = (v1, v0); }
            // v0, v1, v2 now in order by Y
            //   v0 = top (smallest) Y
            //   v1 = middle Y
            //   v2 = bottom (largest) Y

            if (v0.Position.Y == v1.Position.Y) // natural flat top
            {
                // sort top vertices by X
                if (v1.Position.X < v0.Position.X) { (v0, v1) = (v1, v0); }
                // v0 = top left X
                // v1 = top right X
                // v2 = bottom
                DrawFlatTopTriangleTexWrap(v0, v1, v2, texturePixels);
            }
            else if (v1.Position.Y == v2.Position.Y) // natural flat bottom
            {
                // sort bottom vertices by X
                if (v2.Position.X < v1.Position.X) { (v1, v2) = (v2, v1); }
                // v0 = top
                // v1 = bottom left X
                // v2 = bottom right X
                DrawFlatBottomTriangleTexWrap(v0, v1, v2, texturePixels);
            }
            else // general triangle
            {
                // find splitting vertex (vα)

                //               v0
                // -------------*--------- 1
                //       v1        vα      ↑
                // -----*---------*------- 
                //                   v2    ↓
                // -----------------*----- 0
                // The closer v1's Y is to v0's Y, the closer vα will be to 0.
                // The closer v1's Y is to v2's Y, the closer vα will be to 1.
                var αPercent = (v1.Position.Y - v0.Position.Y) / (v2.Position.Y - v0.Position.Y);

                // v2 - v0 is a vector pointing in the direction and length of v0->v2
                // (v2 - v0) * αPercent is a vector pointing in the direction of v0->v2, but with enough length to have the same y as v1.
                // v0 + (v2 - v0) * αPercent is the coordinate on the line v0->v2 that is horizontal to v1 (denoted with vα above).
                var vα = v0.InterpolateTo(v2, αPercent);

                var splitLeft = v1;
                var splitRight = vα;
                if (splitRight.Position.X < splitLeft.Position.X) // major left
                {
                    (splitLeft, splitRight) = (splitRight, splitLeft);
                }
                DrawFlatBottomTriangleTexWrap(v0, splitLeft, splitRight, texturePixels);
                DrawFlatTopTriangleTexWrap(splitLeft, splitRight, v2, texturePixels);
                DrawLine(splitLeft.Position, splitRight.Position, Color.Black);
            }
        }

        private void DrawFlatTopTriangle(Vec2 topLeft, Vec2 topRight, Vec2 bottom, Color color)
        {
            // calculate slopes of the left and right sides of the triangle.
            // Calulate as ΔX / ΔY instead of the traditional ΔY / ΔX.
            // This is because our left and right sides COULD be vertical (ΔX would be 0), but never horizontal.
            // If we did ΔY / ΔX and ΔX was 0, we'd get infinity.  Since our left and right sides will never
            //   be horizontal, ΔX / ΔY is safer for us.
            var leftChangeInXPerY = (bottom.X - topLeft.X) / (bottom.Y - topLeft.Y);
            var rightChangeInXPerY = (bottom.X - topRight.X) / (bottom.Y - topRight.Y);

            // Calculate start and end Y.
            // Subtract 0.5 before Ceiling because of Direct X 11's top-left rasteriztion rule
            //   https://learn.microsoft.com/en-us/windows/win32/direct3d11/d3d10-graphics-programming-guide-rasterizer-stage-rules#triangle-rasterization-rules-without-multisampling
            var yStart = (int)Math.Ceiling(topLeft.Y - 0.5);
            var yEnd = (int)Math.Ceiling(bottom.Y - 0.5); // the scanline AFTER the last line drawn

            // Loop from top Y to (but not including) bottom Y
            for (int y = yStart; y < yEnd; y++)
            {
                // Calculate start and end X
                // add 0.5 to Y value because we're calculating based on pixel CENTERS
                // (currentY + 0.5 - top.Y)                         is our currentY's Δ from the top.
                // ChangeInXPerY * (currentY + 0.5 - top.Y)         is how much the top's X changes to get down the left or right side to currentY.
                // ChangeInXPerY * (currentY + 0.5 - top.Y) + top.X is the X coordinate at currentY for the left or right side.
                var leftX = leftChangeInXPerY * (y + 0.5 - topLeft.Y) + topLeft.X;
                var rightX = rightChangeInXPerY * (y + 0.5 - topRight.Y) + topRight.X;

                // Calculate start and end X
                // Subtract 0.5 before Ceiling because of Direct X 11's top-left rasteriztion rule
                //   https://learn.microsoft.com/en-us/windows/win32/direct3d11/d3d10-graphics-programming-guide-rasterizer-stage-rules#triangle-rasterization-rules-without-multisampling
                var xStart = (int)Math.Ceiling(leftX - 0.5);
                var xEnd = (int)Math.Ceiling(rightX - 0.5); // the pixel AFTER the last pixel drawn

                // Loop from left X to (but not including) right X
                for (int x = xStart; x < xEnd; x++)
                {
                    PutPixel(x, y, color);
                }
            }
        }

        private void DrawFlatBottomTriangle(Vec2 top, Vec2 bottomLeft, Vec2 bottomRight, Color color)
        {
            // calculate slopes of the left and right sides of the triangle.
            // Calulate as ΔX / ΔY instead of the traditional ΔY / ΔX.
            // This is because our left and right sides COULD be vertical (ΔX would be 0), but never horizontal.
            // If we did ΔY / ΔX and ΔX was 0, we'd get infinity.  Since our left and right sides will never
            //   be horizontal, ΔX / ΔY is safer for us.
            var leftChangeInXPerY = (bottomLeft.X - top.X) / (bottomLeft.Y - top.Y);
            var rightChangeInXPerY = (bottomRight.X - top.X) / (bottomRight.Y - top.Y);

            // Calculate start and end Y.
            // Subtract 0.5 before Ceiling because of Direct X 11's top-left rasteriztion rule
            //   https://learn.microsoft.com/en-us/windows/win32/direct3d11/d3d10-graphics-programming-guide-rasterizer-stage-rules#triangle-rasterization-rules-without-multisampling
            var yStart = (int)Math.Ceiling(top.Y - 0.5);       // Top Left Rule states this last line IS rasterized as part of this triangle.
            var yEnd = (int)Math.Ceiling(bottomRight.Y - 0.5); // Top Left Rule states this last line IS NOT rasterized as part of this triangle.

            // Loop from top Y to (but not including) bottom Y
            for (int currentY = yStart; currentY < yEnd; currentY++)
            {
                // Calculate start and end X
                // add 0.5 to Y value because we're calculating based on pixel CENTERS
                // (currentY + 0.5 - top.Y)                         is our currentY's Δ from the top.
                // ChangeInXPerY * (currentY + 0.5 - top.Y)         is how much the top's X changes to get down the left or right side to currentY.
                // ChangeInXPerY * (currentY + 0.5 - top.Y) + top.X is the X coordinate at currentY for the left or right side.
                var leftX = leftChangeInXPerY * (currentY + 0.5 - top.Y) + top.X;
                var rightX = rightChangeInXPerY * (currentY + 0.5 - top.Y) + top.X;

                // Calculate start and end X
                // Subtract 0.5 before Ceiling because of Direct X 11's top-left rasteriztion rule
                //   https://learn.microsoft.com/en-us/windows/win32/direct3d11/d3d10-graphics-programming-guide-rasterizer-stage-rules#triangle-rasterization-rules-without-multisampling
                var xStart = (int)Math.Ceiling(leftX - 0.5);
                var xEnd = (int)Math.Ceiling(rightX - 0.5); // the pixel AFTER the last pixel drawn

                // Loop from left X to (but not including) right X
                for (int currentX = xStart; currentX < xEnd; currentX++)
                {
                    PutPixel(currentX, currentY, currentX % 3 == 0 && currentY % 3 == 0 ? Color.Gray : color);
                }
            }
        }

        private void DrawFlatTopTriangleTex(TexVertex topLeft, TexVertex topRight, TexVertex bottom, TexturePixels texturePixels)
        {
            // bottom.Position.Y - topLeft.Position.Y is the distance from the top to the bottom of the triangle.
            // (bottom - topLeft) is a vertex going from the topLeft to the bottom
            // (bottom - topLeft) / ΔY amount of the vertex going from the topLeft to the bottom needed to advance one Y unit down.
            var ΔY = bottom.Position.Y - topLeft.Position.Y;
            var ΔLeft = (bottom - topLeft) / ΔY;
            var ΔRight = (bottom - topRight) / ΔY;

            // Call the flat triangle render routine
            DrawFlatTriangleTex(topLeft, topRight, bottom, texturePixels, ΔLeft, ΔRight, topRight);
        }

        private void DrawFlatBottomTriangleTex(TexVertex top, TexVertex bottomLeft, TexVertex bottomRight, TexturePixels texturePixels)
        {
            // bottomRight.Position.Y - top.Position.Y is the distance from the top to the bottom of the triangle.
            // (bottomLeft - top) is a vertex going from the top to the bottomLeft
            // (bottomLeft - top) / ΔY amount of the vertex going from the top to the bottomLeft needed to advance one Y unit down.
            var ΔY = bottomRight.Position.Y - top.Position.Y;
            var ΔLeft = (bottomLeft - top) / ΔY;
            var ΔRight = (bottomRight - top) / ΔY;

            // Call the flat triangle render routine
            DrawFlatTriangleTex(top, bottomLeft, bottomRight, texturePixels, ΔLeft, ΔRight, top, true);

        }

        private void DrawFlatTriangleTex(TexVertex topOrTopLeft, TexVertex v1, TexVertex bottomOrBottomRight, TexturePixels texturePixels, TexVertex ΔLeft, TexVertex ΔRight, TexVertex topOrTopRight, bool shouldShade = false)
        {
            var currentPositionOnLeft = topOrTopLeft;
            var currentPositionOnRight = topOrTopRight;

            // Calculate start and end Y.
            // Subtract 0.5 before Ceiling because of Direct X 11's top-left rasteriztion rule
            //   https://learn.microsoft.com/en-us/windows/win32/direct3d11/d3d10-graphics-programming-guide-rasterizer-stage-rules#triangle-rasterization-rules-without-multisampling
            var yStart = (int)Math.Ceiling(topOrTopLeft.Position.Y - 0.5);
            var yEnd = (int)Math.Ceiling(bottomOrBottomRight.Position.Y - 0.5); // the scanline AFTER the last line drawn

            // Do interpolant prestep
            currentPositionOnLeft += ΔLeft * (yStart + 0.5 - topOrTopLeft.Position.Y);
            currentPositionOnRight += ΔRight * (yStart + 0.5 - topOrTopLeft.Position.Y);

            // Initial texture width/height and clamp values
            var tex_width = (double)texturePixels.Width;
            var tex_height = (double)texturePixels.Height;
            var tex_clamp_x = tex_width - 1.0;
            var tex_clamp_y = tex_height - 1.0;

            for (int y = yStart; y < yEnd; y++, currentPositionOnLeft += ΔLeft, currentPositionOnRight += ΔRight)
            {
                // Calculate start and end pixels
                var xStart = (int)Math.Ceiling(currentPositionOnLeft.Position.X - 0.5);
                var xEnd = (int)Math.Ceiling(currentPositionOnRight.Position.X - 0.5); // the pixel AFTER the last pixel drawn

                // (currentRight.Position.X - currentLeft.Position.X) is the distance currentRight's X is from currentLeft's X.
                // (currentRight.TextureCoord - currentLeft.TextureCoord) is a vector pointing from currentLeft to currentRight.
                // (currentRight.TextureCoord - currentLeft.TextureCoord) / ΔX is the amount of vector needed to increment along the horizontal line for each unit of X increment.
                var ΔX = (currentPositionOnRight.Position.X - currentPositionOnLeft.Position.X);
                var ΔLine = (currentPositionOnRight.TextureCoord - currentPositionOnLeft.TextureCoord) / ΔX;

                // Create scanline tex coord interpolant and prestep
                var currentPositionOnLine = currentPositionOnLeft.TextureCoord + ΔLine * (xStart + 0.5 - currentPositionOnLeft.Position.X);

                for (int x = xStart; x < xEnd; x++, currentPositionOnLine += ΔLine)
                {
                    PutPixel(
                        x
                        , y
                        , shouldShade && x % 2 == 0 && y % 2 == 0
                            ? Color.Black
                            : texturePixels.Pixels.GetPixel(
                                (int)Math.Min(currentPositionOnLine.X * tex_width, tex_clamp_x)
                                , (int)Math.Min(currentPositionOnLine.Y * tex_height, tex_clamp_y)
                                , texturePixels.Width
                            )
                    );
                }
            }
        }

        private void DrawFlatTopTriangleTexWrap(TexVertex v0, TexVertex v1, TexVertex v2, TexturePixels texturePixels)
        {
            // calulcate dVertex / dy
            var delta_y = v2.Position.Y - v0.Position.Y;
            var dv0 = (v2 - v0) / delta_y;
            var dv1 = (v2 - v1) / delta_y;

            // create right edge interpolant
            var itEdge1 = v1;

            // call the flat triangle render routine
            DrawFlatTriangleTexWrap(v0, v1, v2, texturePixels, dv0, dv1, itEdge1);
        }

        void DrawFlatBottomTriangleTexWrap(TexVertex v0, TexVertex v1, TexVertex v2, TexturePixels texturePixels)
        {
            // calulcate dVertex / dy
            var delta_y = v2.Position.Y - v0.Position.Y;
            var dv0 = (v1 - v0) / delta_y;
            var dv1 = (v2 - v0) / delta_y;

            // create right edge interpolant
            var itEdge1 = v0;

            // call the flat triangle render routine
            DrawFlatTriangleTexWrap(v0, v1, v2, texturePixels, dv0, dv1, itEdge1, true);
        }

        private void DrawFlatTriangleTexWrap(TexVertex v0, TexVertex v1, TexVertex v2, TexturePixels texturePixels,
                                            TexVertex dv0, TexVertex dv1, TexVertex itEdge1, bool shouldShade = false)
        {
            // Create edge interpolant for left edge (always v0)
            TexVertex itEdge0 = v0;

            // calculate start and end scanlines
            var yStart = (int)Math.Ceiling(v0.Position.Y - 0.5);
            var yEnd = (int)Math.Ceiling(v2.Position.Y - 0.5); // the scanline AFTER the last line drawn

            // Do interpolant prestep
            itEdge0 += dv0 * (yStart + 0.5 - v0.Position.Y);
            itEdge1 += dv1 * (yStart + 0.5 - v0.Position.Y);

            // Initial texture width/height and clamp values
            var tex_width = (double)texturePixels.Width;
            var tex_height = (double)texturePixels.Height;
            var tex_clamp_x = tex_width - 1.0;
            var tex_clamp_y = tex_height - 1.0;

            for (int y = yStart; y < yEnd; y++, itEdge0 += dv0, itEdge1 += dv1)
            {
                // Calculate start and end pixels
                var xStart = (int)Math.Ceiling(itEdge0.Position.X - 0.5);
                var xEnd = (int)Math.Ceiling(itEdge1.Position.X - 0.5); // the pixel AFTER the last pixel drawn

                // Calculate scanline dTexCoord / dx
                var dtcLine = (itEdge1.TextureCoord - itEdge0.TextureCoord) / (itEdge1.Position.X - itEdge0.Position.X);

                // Create scanline tex coord interpolant and prestep
                var itcLine = itEdge0.TextureCoord + dtcLine * (xStart + 0.5 - itEdge0.Position.X);

                for (int x = xStart; x < xEnd; x++, itcLine += dtcLine)
                {
                    PutPixel(
                        x
                        , y
                        , shouldShade && x % 2 == 0 && y % 2 == 0
                            ? Color.Black
                            : texturePixels.Pixels.GetPixel(
                                (int)(itcLine.X * tex_width % tex_clamp_x)
                                , (int)(itcLine.Y * tex_height % tex_clamp_y)
                                , texturePixels.Width
                            )
                    );
                }
            }
        }
    }
}
