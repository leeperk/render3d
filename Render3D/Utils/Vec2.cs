﻿using System;

namespace Render3D.Utils
{
    public class Vec2
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Vec2() { }

        public Vec2(double x, double y)
        {
            X = x;
            Y = y;
        }

        public Vec2(Vec2 other) : this(other.X, other.Y) { }

        public virtual double LenSq()
        {
            return this * this;
        }

        public virtual double Len()
        {
            return (double)Math.Sqrt(LenSq());
        }

        public virtual Vec2 Normalize()
        {
            var length = Len();
            X /= length;
            Y /= length;
            return this;
        }

        public virtual Vec2 GetNormalize()
        {
            var norm = this;
            norm.Normalize();
            return norm;
        }

        public Vec2 InterpolateTo(Vec2 destination, double αPercent)
            => this + (destination - this) * αPercent;

        public static Vec2 operator -(Vec2 rhs)
            => new Vec2(-rhs.X, -rhs.Y);

        public static Vec2 operator +(Vec2 lhs, Vec2 rhs)
            => new Vec2(lhs.X + rhs.X, lhs.Y + rhs.Y);

        public static Vec2 operator -(Vec2 lhs, Vec2 rhs)
            => new Vec2(lhs.X - rhs.X, lhs.Y - rhs.Y);

        public static double operator *(Vec2 lhs, Vec2 rhs)
            => lhs.X * rhs.X + lhs.Y * rhs.Y;

        public static Vec2 operator *(Vec2 lhs, double rhs)
            => new Vec2(lhs.X * rhs, lhs.Y * rhs);

        public static Vec2 operator /(Vec2 lhs, double rhs)
            => new Vec2(lhs.X / rhs, lhs.Y / rhs);

        public static bool operator ==(Vec2 lhs, Vec2 rhs)
            => lhs.X == rhs.X && lhs.Y == rhs.Y;

        public static bool operator !=(Vec2 lhs, Vec2 rhs)
            => false == (lhs == rhs);

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj is null || obj.GetType() != GetType())
            {
                return false;
            }

            return this == (Vec2)obj;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y);
        }
    }
}
