﻿using System;

namespace Render3D.Utils
{
    public class Vec3 : Vec2
    {
        public double Z { get; set; }

        public Vec3() { }

        public Vec3(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Vec3(Vec3 other) : this(other.X, other.Y, other.Z) { }
        
        public override Vec3 Normalize()
        {
            var length = Len();
            X /= length;
            Y /= length;
            Z /= length;
            return this;
        }

        public Vec3 InterpolateTo(Vec3 destination, double αPercent)
            => this + (destination - this) * αPercent;

        public static Vec3 operator -(Vec3 rhs)
            => new Vec3(-rhs.X, -rhs.Y, -rhs.Z);

        public static Vec3 operator +(Vec3 lhs, Vec3 rhs)
            => new Vec3(lhs.X + rhs.X, lhs.Y + rhs.Y, lhs.Z + rhs.Z);

        public static Vec3 operator -(Vec3 lhs, Vec3 rhs)
            => new Vec3(lhs.X - rhs.X, lhs.Y - rhs.Y, lhs.Z - rhs.Z);

        public static double operator *(Vec3 lhs, Vec3 rhs)
            => lhs.X * rhs.X + lhs.Y * rhs.Y + lhs.Z * rhs.Z;

        public static Vec3 operator *(Vec3 lhs, Mat3 rhs)
            => new Vec3(
                  lhs.X * rhs.Elements[0, 0] + lhs.Y * rhs.Elements[1, 0] + lhs.Z * rhs.Elements[2, 0]
                , lhs.X * rhs.Elements[0, 1] + lhs.Y * rhs.Elements[1, 1] + lhs.Z * rhs.Elements[2, 1]
                , lhs.X * rhs.Elements[0, 2] + lhs.Y * rhs.Elements[1, 2] + lhs.Z * rhs.Elements[2, 2]
            );

        public static Vec3 operator *(Vec3 lhs, double rhs)
            => new Vec3(lhs.X * rhs, lhs.Y * rhs, lhs.Z * rhs);

        public static Vec3 operator %(Vec3 lhs, Vec3 rhs)
            => new Vec3(
                lhs.Y * rhs.Z - lhs.Z * rhs.Y
                , lhs.Z * rhs.X - lhs.X * rhs.Z
                , lhs.X * rhs.Y - lhs.Y * rhs.X
            );

        public static Vec3 operator /(Vec3 lhs, double rhs)
            => new Vec3(lhs.X / rhs, lhs.Y / rhs, lhs.Z / rhs);

        public static bool operator ==(Vec3 lhs, Vec3 rhs)
            => lhs.X == rhs.X && lhs.Y == rhs.Y && lhs.Z == rhs.Z;

        public static bool operator !=(Vec3 lhs, Vec3 rhs)
            => false == (lhs == rhs);

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj is null || obj.GetType() != GetType())
            {
                return false;
            }

            return this == (Vec3)obj;
        }

        public override int GetHashCode() => HashCode.Combine(X, Y, Z);
    }
}
