﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Render3D.Models;
using Render3D.Utils;
using System;

namespace Render3D.Scenes
{
    public class ConHexScene : Scene
    {
        private PerspectiveTransformer _perspectiveTransformer;
        private ConcaveHexhedron _hex = new ConcaveHexhedron(1.0);
        private Color[] _colors = new[] {
                Color.White,
                Color.Blue,
                Color.Cyan,
                Color.Yellow,
                Color.Green,
                Color.Magenta
            };
        private readonly Canvas _canvas;
        private const double _dTheta = Math.PI;
        private double _offsetZ = 2.0;
        private double _thetaX = 0.0;
        private double _thetaY = 0.0;
        private double _thetaZ = 0.0;

        public ConHexScene(Canvas canvas): base("Concave Hexahedron Nasty Draw Order")
        {
            _canvas = canvas;
            _perspectiveTransformer = new PerspectiveTransformer(_canvas.Width, _canvas.Height);
        }

        public override void Update(GameTime gameTime)
        {
            var deltaSeconds = gameTime.ElapsedGameTime.TotalSeconds;
            var keyboardState = Keyboard.GetState();
            if (keyboardState.IsKeyDown(Keys.Q))
            {
                _thetaX = WrapAngle(_thetaX + _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.W))
            {
                _thetaY = WrapAngle(_thetaY + _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.E))
            {
                _thetaZ = WrapAngle(_thetaZ + _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.A))
            {
                _thetaX = WrapAngle(_thetaX - _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.S))
            {
                _thetaY = WrapAngle(_thetaY - _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.D))
            {
                _thetaZ = WrapAngle(_thetaZ - _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.R))
            {
                _offsetZ += 2.0 * deltaSeconds;
            }
            if (keyboardState.IsKeyDown(Keys.F))
            {
                _offsetZ -= 2.0 * deltaSeconds;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _canvas.BeginFrame();
            ComposeFrame();
            _canvas.EndFrame();
        }

        private void ComposeFrame()
        {
            // Generate index triangle list
            var triangles = _hex.GetTriangles();
            // Generate rotation matrix from euler angles
            var rot =
                Mat3.RotationX(_thetaX)
                * Mat3.RotationY(_thetaY)
                * Mat3.RotationZ(_thetaZ);
            // Transform from model space -> world (/view) space
            for (var i = 0; i < triangles.Vertices.Count; i++)
            {
                triangles.Vertices[i] *= rot;
                triangles.Vertices[i] += new Vec3(0.0, 0.0, _offsetZ);
            }
            // Backface culling test (must be done in world (/view) space)
            for (int i = 0, end = triangles.Indices.Count / 3; i < end; i++)
            {
                var v0 = triangles.Vertices[triangles.Indices[i * 3]];
                var v1 = triangles.Vertices[triangles.Indices[i * 3 + 1]];
                var v2 = triangles.Vertices[triangles.Indices[i * 3 + 2]];
                triangles.CullFlags[i] = (v1 - v0) % (v2 - v0) * v0 > 0.0;
            }

            // Transform to screen space (includes perspective transform)
            for (var i = 0; i < triangles.Vertices.Count; i++)
            {
                _perspectiveTransformer.Transform(triangles.Vertices[i]);
            }

            // Draw the triangles
            for (int i = 0, end = triangles.Indices.Count / 3; i < end; i++)
            {
                // skip triangles previously determined to be back-facing
                if (false == triangles.CullFlags[i])
                {
                    _canvas.DrawTriangle(
                        triangles.Vertices[triangles.Indices[i * 3]]
                        , triangles.Vertices[triangles.Indices[i * 3 + 1]]
                        , triangles.Vertices[triangles.Indices[i * 3 + 2]]
                        , _colors[i]
                    );
                }
            }
        }
    }
}
