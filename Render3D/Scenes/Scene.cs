﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;

namespace Render3D.Scenes
{
    public abstract class Scene
    {
        private string _name;

        protected Scene(string name)
        {
            _name = name;
        }

        public abstract void Update(GameTime gameTime);
        public abstract void Draw(GameTime gameTime);
        public virtual void LoadContent(ContentManager content) { }

        public virtual string GetName() => _name;

        protected double WrapAngle(double theta)
        {
            var modded = theta % (2 * Math.PI);
            return modded > Math.PI ? modded - 2 * Math.PI : modded;
        }
    }
}
