﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Render3D.Models;
using Render3D.Utils;
using System;

namespace Render3D.Scenes
{
    public class ConHexWireScene : Scene
    {
        private PerspectiveTransformer _perspectiveTransformer;
        private ConcaveHexhedron _hex = new ConcaveHexhedron(1.0);
        private Color _colorSolid = Color.Gray;
        private Color _colorWire = Color.Magenta;
        private readonly Canvas _canvas;
        private const double _dTheta = Math.PI;
        private double _offsetZ = 2.0;
        private double _thetaX = 0.0;
        private double _thetaY = 0.0;
        private double _thetaZ = 0.0;

        public ConHexWireScene(Canvas canvas): base("Concave Hexahedron Filled Wireframe")
        {
            _canvas = canvas;
            _perspectiveTransformer = new PerspectiveTransformer(_canvas.Width, _canvas.Height);
        }

        public override void Update(GameTime gameTime)
        {
            var deltaSeconds = gameTime.ElapsedGameTime.TotalSeconds;
            var keyboardState = Keyboard.GetState();
            if (keyboardState.IsKeyDown(Keys.Q))
            {
                _thetaX = WrapAngle(_thetaX + _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.W))
            {
                _thetaY = WrapAngle(_thetaY + _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.E))
            {
                _thetaZ = WrapAngle(_thetaZ + _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.A))
            {
                _thetaX = WrapAngle(_thetaX - _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.S))
            {
                _thetaY = WrapAngle(_thetaY - _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.D))
            {
                _thetaZ = WrapAngle(_thetaZ - _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.R))
            {
                _offsetZ += 2.0 * deltaSeconds;
            }
            if (keyboardState.IsKeyDown(Keys.F))
            {
                _offsetZ -= 2.0 * deltaSeconds;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _canvas.BeginFrame();
            ComposeFrame();
            _canvas.EndFrame();
        }

        private void ComposeFrame()
        {
            // Generate index triangle list
            var triangles = _hex.GetTriangles();
            var lines = _hex.GetLines();
            // Generate rotation matrix from euler angles
            var rot =
                Mat3.RotationX(_thetaX)
                * Mat3.RotationY(_thetaY)
                * Mat3.RotationZ(_thetaZ);
            // Transform from model space -> world (/view) space
            for (var i = 0; i < triangles.Vertices.Count; i++)
            {
                triangles.Vertices[i] *= rot;
                triangles.Vertices[i] += new Vec3(0.0, 0.0, _offsetZ);
            }
            for (var i = 0; i < lines.Vertices.Count; i++)
            {
                lines.Vertices[i] *= rot;
                lines.Vertices[i] += new Vec3(0.0, 0.0, _offsetZ);
            }
            // Transform to screen space (includes perspective transform)
            for (var i = 0; i < triangles.Vertices.Count; i++)
            {
                _perspectiveTransformer.Transform(triangles.Vertices[i]);
            }
            for (var i = 0; i < lines.Vertices.Count; i++)
            {
                _perspectiveTransformer.Transform(lines.Vertices[i]);
            }
            // Draw the triangles
            for (int i = 0, end = triangles.Indices.Count / 3; i < end; i++)
            {
                _canvas.DrawTriangle(
                    triangles.Vertices[triangles.Indices[i * 3]]
                    , triangles.Vertices[triangles.Indices[i * 3 + 1]]
                    , triangles.Vertices[triangles.Indices[i * 3 + 2]]
                    , _colorSolid
                );
            }
            // Draw the lines
            for (int i = 0, end = lines.Indices.Count / 2; i < end; i++)
            {
                _canvas.DrawLine(
                    lines.Vertices[lines.Indices[i * 2]]
                    , lines.Vertices[lines.Indices[i * 2 + 1]]
                    , _colorWire
                );
            }
        }
    }
}
