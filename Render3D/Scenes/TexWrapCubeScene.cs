﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Render3D.Models;
using Render3D.Utils;
using System;

namespace Render3D.Scenes
{
    public class TexWrapCubeScene : Scene
    {
        private PerspectiveTransformer _perspectiveTransformer;
        private readonly Canvas _canvas;
        private readonly Cube _cube;
        private readonly string _imageName;
        private TexturePixels _texture;
        private const double _dTheta = Math.PI;
        private double _offsetZ = 2.0;
        private double _thetaX = 0.0;
        private double _thetaY = 0.0;
        private double _thetaZ = 0.0;

        public TexWrapCubeScene(Canvas canvas, string imageName, double texdim) : this(canvas, texdim)
        {
            _imageName = imageName;
        }

        public TexWrapCubeScene(Canvas canvas, double texdim): base($"Textured Cube Sauron Wrapping Dim: {texdim}")
        {
            _canvas = canvas;
            _perspectiveTransformer = new PerspectiveTransformer(_canvas.Width, _canvas.Height);
            _cube = new Cube(1.0, texdim);
            _imageName = "sauron-100x100";
        }

        public override void LoadContent(ContentManager content)
        {
            _texture = new TexturePixels(content.Load<Texture2D>(_imageName));
        }

        public override void Update(GameTime gameTime)
        {
            var deltaSeconds = gameTime.ElapsedGameTime.TotalSeconds;
            var keyboardState = Keyboard.GetState();
            if (keyboardState.IsKeyDown(Keys.Q))
            {
                _thetaX = WrapAngle(_thetaX + _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.W))
            {
                _thetaY = WrapAngle(_thetaY + _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.E))
            {
                _thetaZ = WrapAngle(_thetaZ + _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.A))
            {
                _thetaX = WrapAngle(_thetaX - _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.S))
            {
                _thetaY = WrapAngle(_thetaY - _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.D))
            {
                _thetaZ = WrapAngle(_thetaZ - _dTheta * deltaSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.R))
            {
                _offsetZ += 2.0 * deltaSeconds;
            }
            if (keyboardState.IsKeyDown(Keys.F))
            {
                _offsetZ -= 2.0 * deltaSeconds;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _canvas.BeginFrame();
            ComposeFrame();
            _canvas.EndFrame();
        }

        private void ComposeFrame()
        {
            // Generate index triangle list
            var triangles = _cube.GetTrianglesTex();
            // Generate rotation matrix from euler angles
            var rot =
                Mat3.RotationX(_thetaX)
                * Mat3.RotationY(_thetaY)
                * Mat3.RotationZ(_thetaZ);
            // Transform from model space -> world (/view) space
            for (var i = 0; i < triangles.Vertices.Count; i++)
            {
                triangles.Vertices[i].Position *= rot;
                triangles.Vertices[i].Position += new Vec3(0.0, 0.0, _offsetZ);
            }
            // Backface culling test (must be done in world (/view) space)
            for (int i = 0, end = triangles.Indices.Count / 3; i < end; i++)
            {
                var v0 = triangles.Vertices[triangles.Indices[i * 3]].Position;
                var v1 = triangles.Vertices[triangles.Indices[i * 3 + 1]].Position;
                var v2 = triangles.Vertices[triangles.Indices[i * 3 + 2]].Position;
                triangles.CullFlags[i] = (v1 - v0) % (v2 - v0) * v0 > 0.0;
            }

            // Transform to screen space (includes perspective transform)
            for (var i = 0; i < triangles.Vertices.Count; i++)
            {
                _perspectiveTransformer.Transform(triangles.Vertices[i].Position);
            }

            // Draw the triangles
            for (int i = 0, end = triangles.Indices.Count / 3; i < end; i++)
            {
                // skip triangles previously determined to be back-facing
                if (false == triangles.CullFlags[i])
                {
                    _canvas.DrawTriangleTexWrap(
                        triangles.Vertices[triangles.Indices[i * 3]]
                        , triangles.Vertices[triangles.Indices[i * 3 + 1]]
                        , triangles.Vertices[triangles.Indices[i * 3 + 2]]
                        , _texture
                    );
                }
            }
        }
    }
}
