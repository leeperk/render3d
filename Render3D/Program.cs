﻿using Render3D;

public class Program
{

    static void Main(string[] args)
    {
        using var game = new Game1();
        game.Run();
    }
}
