﻿using Render3D.Utils;
using System.Collections.Generic;
using System.Linq;

namespace Render3D.Models
{
    public class ConcaveHexhedron
    {
        private IList<Vec3> _vertices;

        public ConcaveHexhedron(double size)
        {
            var side = size / 2.0;
            _vertices = new List<Vec3>()
            {
                  new Vec3( 0.0,   side,  0.0 )
                , new Vec3(-side, -side,  0.0 )
                , new Vec3( side, -side,  0.0 )
                , new Vec3( 0.0,   0.0,   side)
                , new Vec3( 0.0,   0.0,  -side)
            };
        }

        public IndexedLineList GetLines()
        {
            return new IndexedLineList()
            {
                Vertices = _vertices.ToList()
                ,
                Indices =
                {
                    0, 1,  1, 3,  3, 2,  2, 0,
                    3, 0,  3, 4,  1, 4,  2, 4,  4, 0
                }
            };
        }

        public IndexedTriangleList<Vec3> GetTriangles()
        {
            return new IndexedTriangleList<Vec3>(
                _vertices.ToList()
                , new[] {
                    1,0,3, 3,0,2,
                    1,4,0, 4,2,0,
                    3,4,1, 4,3,2
                }
            );
        }
    }
}
