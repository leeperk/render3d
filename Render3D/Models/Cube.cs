﻿using Render3D.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Render3D.Models
{
    public class Cube
    {
        private IList<Vec3> _vertices;
        private IList<Vec2> _textureCoords;

        public Cube(double size, double texdim = 1.0)
        {
            var side = size / 2.0;
            _vertices = new List<Vec3>()
            {
                  new Vec3(-side, -side, -side)
                , new Vec3( side, -side, -side)
                , new Vec3(-side,  side, -side)
                , new Vec3( side,  side, -side)
                , new Vec3(-side, -side,  side)
                , new Vec3( side, -side,  side)
                , new Vec3(-side,  side,  side)
                , new Vec3( side,  side,  side)
            };
            _textureCoords = new List<Vec2>()
            {
                  new Vec2 (0.0, texdim)
                , new Vec2 (texdim, texdim)
                , new Vec2 (0.0, 0.0)
                , new Vec2 (texdim, 0.0)
                , new Vec2 (texdim, texdim)
                , new Vec2 (0.0, texdim)
                , new Vec2 (texdim, 0.0)
                , new Vec2 (0.0, 0.0)
            };
        }

        public IndexedLineList GetLines()
        {
            return new IndexedLineList()
            {
                Vertices = _vertices.ToList()
                ,
                Indices =
                {
                    0, 1,  1, 3,  3, 2,  2, 0,
                    0, 4,  1, 5,  3, 7,  2, 6,
                    4, 5,  5, 7,  7, 6,  6, 4
                }
            };
        }

        public IndexedTriangleList<Vec3> GetTriangles()
        {
            return new IndexedTriangleList<Vec3>(
                _vertices.ToList()
                , new[] {
                    0,2,1, 2,3,1,
                    1,3,5, 3,7,5,
                    2,6,3, 3,6,7,
                    4,5,7, 4,7,6,
                    0,4,2, 2,4,6,
                    0,1,4, 1,5,4
                }
            );
        }

        public IndexedTriangleList<TexVertex> GetTrianglesTex()
        {
            var tverts = new List<TexVertex>();
            for (var i = 0; i < _vertices.Count; i++)
            {
                tverts.Add(new TexVertex(_vertices[i], _textureCoords[i]));
            }
            return new IndexedTriangleList<TexVertex>(
                tverts.ToList()
                , new[] {
                        0,2,1, 2,3,1,
                        1,3,5, 3,7,5,
                        2,6,3, 3,6,7,
                        4,5,7, 4,7,6,
                        0,4,2, 2,4,6,
                        0,1,4, 1,5,4
                }
            );
        }
    }
}
