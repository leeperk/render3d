﻿using Render3D.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Render3D.Models
{
    public class XQuads
    {
        private IList<Vec3> _vertices;

        public XQuads(double size)
        {
            var side = size / 2.0;
            _vertices = new List<Vec3>()
            {
                  new Vec3(-side, -side, -side)
                , new Vec3( side, -side, -side)
                , new Vec3(-side,  side, -side)
                , new Vec3( side,  side, -side)
                , new Vec3(-side, -side,  side)
                , new Vec3( side, -side,  side)
                , new Vec3(-side,  side,  side)
                , new Vec3( side,  side,  side)
            };
        }

        public IndexedLineList GetLines()
        {
            return new IndexedLineList()
            {
                Vertices = _vertices.ToList()
                ,
                Indices =
                {
                    0,4, 4,7, 7,3, 3,0,
                    1,5, 5,6, 6,2, 2,1
                }
            };
        }

        public IndexedTriangleList<Vec3> GetTriangles()
        {
            return new IndexedTriangleList<Vec3>(
                _vertices.ToList()
                , new[] {
                    0,4,3, 4,7,3,
                    2,6,1, 6,5,1
                }
            );
        }
    }
}
