﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Render3D.Extensions;
using Render3D.Utils;
using Render3D.Scenes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Render3D
{
    public class Game1 : Game
    {
        private readonly Canvas _canvas;
        private SpriteBatch _spriteBatch;
        private SpriteFont _font;
        private int _frameCount;
        private double _frameTimer;

        private IList<Scene> _scenes;
        private int _currentSceneIndex;

        public Game1()
        {
            _canvas = new Canvas(new GraphicsDeviceManager(this));
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            _scenes = new List<Scene>()
            {
                new SolidCubeScene(_canvas)
                , new CubeOrderScene(_canvas)
                , new ConHexScene(_canvas)
                , new ConHexWireScene(_canvas)
                , new XMutualScene(_canvas)
                , new TexCubeScene(_canvas)
                , new TexCubeScene(_canvas, 2.0)
                , new TexWrapCubeScene(_canvas, 2.0)
                , new TexWrapCubeScene(_canvas, "wood", 2.0)
                , new TexWrapCubeScene(_canvas, 6.0)
                , new FoldedCubeScene(_canvas)
                , new FoldedCubeWrapScene(_canvas)
                , new CubeSkinnedScene(_canvas, "dice_skin")
                , new CubeSkinnedScene(_canvas, "office_skin")
                , new CubeSkinnedScene(_canvas, "office_skin_lores")
            };
            _currentSceneIndex = 0;
        }

        protected override void Initialize()
        {
            IsFixedTimeStep = false;
            base.Initialize();
            _canvas.Initialize();

            _frameTimer = 0.0;
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            foreach(var scene in _scenes)
            {
                scene.LoadContent(Content);
            }
            _font = Content.Load<SpriteFont>("Score");
        }

        protected override void Update(GameTime gameTime)
        {
            var deltaSeconds = gameTime.ElapsedGameTime.TotalSeconds;
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Exit();
            }

            _frameCount++;
            _frameTimer += deltaSeconds;
            if (_frameTimer >= 1.0)
            {
                Console.SetCursorPosition(0, 0);
                Console.WriteLine($"FPS: {_frameCount}");
                _frameTimer = _frameCount = 0;
            }

            var keyboardState = Keyboard.GetState();

            // Cycle through scenes when (Shift +) Tab is pressed
            if (keyboardState.WasKeyPressedOnce(Keys.Tab))
            {
                if (keyboardState.IsKeyDown(Keys.LeftShift) || keyboardState.IsKeyDown(Keys.RightShift))
                {
                    ReverseCycleScenes();
                }
                else
                {
                    CycleScenes();
                }
            }

            _scenes[_currentSceneIndex].Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            _scenes[_currentSceneIndex].Draw(gameTime);

            _spriteBatch.Begin();
            _spriteBatch.Draw(_canvas.Texture, new Rectangle(0, 0, _canvas.Width, _canvas.Height), Color.White);
            _spriteBatch.End();

            base.Draw(gameTime);
        }

        private void CycleScenes()
        {
            if (++_currentSceneIndex == _scenes.Count)
            {
                _currentSceneIndex = 0;
            }
            OutputSceneName();
        }

        private void ReverseCycleScenes()
        {
            if (--_currentSceneIndex == -1)
            {
                _currentSceneIndex = _scenes.Count - 1;
            }
            OutputSceneName();
        }

        private void OutputSceneName()
        {
            var sceneName = _scenes[_currentSceneIndex].GetName();
            var stars = new string('*', sceneName.Length + 4);
            Console.SetCursorPosition(0, 1);
            ConsoleWriteAndClearRemainingLine(stars);
            ConsoleWriteAndClearRemainingLine($"* {sceneName} *");
            ConsoleWriteAndClearRemainingLine(stars);
        }

        private static void ConsoleWriteAndClearRemainingLine(string message)
        {
            Console.Write(message);
            (var x, _) = Console.GetCursorPosition();
            Console.Write(new string(' ', Console.BufferWidth - x));
        }
    }
}