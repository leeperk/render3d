﻿using System;

namespace Render3D.Exceptions
{

    public class Render3dException: Exception
    {
        public Render3dException() {}

        public Render3dException(string message): base(message) {}

        public Render3dException(string message, Exception inner): base(message, inner) {}
    }
}
