﻿using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Render3D.Extensions
{
    public static class KeyboardStateExtensions
    {
        private static Dictionary<Keys, bool> _keysPressedLastCheck = new Dictionary<Keys, bool>();

        public static bool WasKeyPressedOnce(this KeyboardState keyboardState, Keys key)
        {
            var isKeyCurrentlyPressed = keyboardState.IsKeyDown(key);
            var wasKeyPressedLastcheck = _keysPressedLastCheck.ContainsKey(key) && _keysPressedLastCheck[key];
            var wasKeyPressedOnce = false == wasKeyPressedLastcheck && isKeyCurrentlyPressed;
            
            _keysPressedLastCheck[key] = isKeyCurrentlyPressed;
            
            return wasKeyPressedOnce;
        }
    }
}
