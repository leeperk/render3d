﻿using Microsoft.Xna.Framework;
using System.Runtime.CompilerServices;

namespace Render3D.Extensions
{
    public static class ColorArrayExtensions
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Color GetPixel(this Color[] pixels, int x, int y, int width)
        {
            return pixels[x + y * width];
        }
    }
}
