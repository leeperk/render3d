﻿using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace ConsoleCanvas;

public class Canvas : IDisposable
{
    [DllImport("kernel32.dll", ExactSpelling = true)]
    private static extern IntPtr GetConsoleWindow();

    [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

    [DllImport("user32.dll")]
    private static extern int GetWindowRect(IntPtr hwnd, out Rect rect);

    [DllImport(@"dwmapi.dll")]
    private static extern int DwmGetWindowAttribute(IntPtr hwnd, int dwAttribute, out Rect pvAttribute, int cbAttribute);

    [DllImport("user32.dll")]
    private static extern int GetSystemMetrics(int nIndex);

    [StructLayout(LayoutKind.Sequential)]
    private struct Rect
    {
        public int Left;
        public int Top;
        public int Right;
        public int Bottom;

        public int Width => Right - Left;
        public int Height => Bottom - Top;
    }

    private const int SM_CYCAPTION = 4;
    private const int SM_CYFRAME = 33;
    private const int SM_CXPADDEDBORDER = 92;
    private const int MAXIMIZE = 3;
    private const int DWMWA_EXTENDED_FRAME_BOUNDS = 9;
    private readonly IntPtr _thisConsole = GetConsoleWindow();

    private Size _windowSize;
    public Bitmap Buffer { get; private set; }
    private bool _disposed;

    public Canvas()
    {
        Console.SetWindowSize(Console.LargestWindowWidth, Console.LargestWindowHeight);
        ShowWindow(_thisConsole, MAXIMIZE);
        Console.CursorVisible = false;

        var handle = GetConsoleWindow();
        DwmGetWindowAttribute(handle, DWMWA_EXTENDED_FRAME_BOUNDS, out Rect rect, Marshal.SizeOf(typeof(Rect)));
        var titleBarHeight = GetSystemMetrics(SM_CYCAPTION);
        _windowSize = new Size(rect.Width, rect.Height - titleBarHeight);
        Buffer?.Dispose();
        Buffer = new Bitmap(_windowSize.Width, _windowSize.Height, PixelFormat.Format32bppArgb);
    }

    public void SetFrame()
    {
        if (Buffer == null)
        {
            return;
        }
        var handle = GetConsoleWindow();
        using (var graphics = Graphics.FromHwnd(handle))
        {
            graphics.DrawImage(Buffer, 0, 0, _windowSize.Width, _windowSize.Height);
        }
    }

    ~Canvas()
    {
        Dispose(false);
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (_disposed)
        {
            return;
        }

        if (disposing)
        {
            if (Buffer != null)
            {
                Buffer.Dispose();
                Buffer = null;
            }
        }
        _disposed = true;
    }
}
